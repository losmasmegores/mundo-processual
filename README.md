# Mundo Processual

Al nostre projecte només hem utilitzat la funció procedural de soroll Perlin.
Primer hem fet servir una funció per determinar l'estructura del món, és a dir, les diferents altures d'aquest. Posteriorment, per crear els diferents biomes, emprem dues funcions Perlin, que mitjançant la temperatura i la precipitació (probabilitat de pluja), decidirem on hi haurà un bioma de muntanya, d'oceà, de desert, de neu i de camp.
Si la temperatura del bioma és menor de 0.15, sempre hi haurà neu.
En cas que la temperatura sigui menor de 0.5, si la precipitació és major de 0.4, serà muntanya, si no serà camp.

Si la temperatura és menor a 0.8:
- Primerament, comprovem si la precipitació és major a 0.8, si és el cas, el bioma serà sempre oceà.
- En cas que la precipitació sigui major a 0.3, el bioma serà camp
- En qualsevol altre cas, comprovarem un altre cop la temperatura, en cas que sigui menor a 0.65, serà camp, si no, serà desert.
Si la temperatura és major a 0.8, en el cas que la precipitació sigui major a 0.5, serà oceà. En qualsevol altre cas, serà desert.

Després de decidir tota l'estructura de biomes, passem a la generació d'objectes a dins d'aquests.
Tenim dos objectes per bioma, menys a neu i oceà.
Cada vegada que es genera un cub, generem un perlin per comprovar la probabilitat de generar un objecte.
Si aquest Perlin és major a 0.85, generarà l'objecte "gran", si no, si el Perlin és major a 0.7, genera l'objecte "petit". Si el Perlin és menor a 0.7, no es genera cap objecte al cub.