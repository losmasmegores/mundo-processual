 using System;
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 [CreateAssetMenu]
public class BiomaInfo : ScriptableObject
{
 [Serializable]
 public struct BiomaInfoStruct
 {
  public Material color;
  public float frequency;
  public float offsetX;
  public float offsetY;
  public GameObject[] props;

 }

 public BiomaInfoStruct info;
}
