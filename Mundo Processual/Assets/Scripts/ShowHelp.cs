using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShowHelp : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI m_Help;
    [SerializeField] TextMeshProUGUI m_Manual;


    private void Awake()
    {
        m_Help.gameObject.SetActive(true);
        m_Manual.gameObject.SetActive(false);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.H))
        {
            m_Help.gameObject.SetActive(!m_Help.gameObject.activeSelf);
            m_Manual.gameObject.SetActive(!m_Manual.gameObject.activeSelf);
        }
    }
}
