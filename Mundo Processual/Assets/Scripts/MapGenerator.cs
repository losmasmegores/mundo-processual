using System;
using Unity.VisualScripting;
using UnityEditor.PackageManager.UI;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace m17
{
    public class MapGenerator : MonoBehaviour
    {

        [SerializeField] private bool m_Verbose = false;

        [SerializeField] private GameObject cube;

        //graphic
        private Terrain m_Terrain;
        [SerializeField] private Gradient m_Gradient;
        [SerializeField] private Material m_TerrainMaterial;
        private Texture2D m_TerrainTexture;
        private Texture2D[] m_Texture;
        private float[][,] m_Heights;

        //internal logic
        private int m_TextureMode = 0;
        private int m_TextureOctave = 0;

        //texture GUI
        [SerializeField] private GameObject m_GUI;

        [Header("Size")]
        //size of the area we will paint
        [SerializeField]
        private int m_Width;

        [SerializeField] private int m_Height;

        [Header("Height Parameters")] 
        [SerializeField]
        //offset from the perlin map
        private float m_OffsetX_height;

        [SerializeField] private float m_OffsetY_height;
        [SerializeField] private float m_Frequency_height = 4f;

        [Header("Temp Parameters")] 
        [SerializeField]
        //offset from the perlin map
        private float m_OffsetX_temp;

        [SerializeField] private float m_OffsetY_temp;
        [SerializeField] private float m_Frequency_temp = 4f;
        
        [Header("Rainy Parameters")] 
        [SerializeField]
        //offset from the perlin map
        private float m_OffsetX_rain;

        [SerializeField] private float m_OffsetY_rain;
        [SerializeField] private float m_Frequency_rain = 4f;
        


        //octaves
        private const int MAX_OCTAVES = 8;

        [Header("Octave Parameters")] [SerializeField] [Range(0, MAX_OCTAVES)]
        private int m_Octaves = 0;

        [Range(2, 3)] [SerializeField] private int m_Lacunarity = 2;
        [SerializeField] [Range(0.1f, 0.9f)] private float m_Persistence = 0.5f;

        [Tooltip("Do the octaves carve the terrain?")] [SerializeField]
        private bool m_Carve = true;

        [Header("Biomas")]
        public BiomaInfo oceano;
        public BiomaInfo cesped;
        public BiomaInfo arena;
        public BiomaInfo montana;
        public BiomaInfo nieve;


        [Header("Multiply")] public float multiply;
        public Transform props;
        private BiomaInfo actualBioma;
        [Header("Percentage (Exclusive)")] public int numMax = 5;
        void Start()
        {
            m_Terrain = GetComponent<Terrain>();

            // m_Height = m_Terrain.terrainData.heightmapTexture.height;
            // m_Width = m_Terrain.terrainData.heightmapTexture.width;

            //Crearem una textura per cada perlin i les seves octaves amb el resultat conjunt
            //i un altre amb el resultat base.
            m_Texture = new Texture2D[(MAX_OCTAVES + 1) * 2];
            for (int i = 0; i < (MAX_OCTAVES + 1) * 2; i++)
            {
                m_Texture[i] = new Texture2D(m_Width, m_Height);
                m_Texture[i].filterMode = FilterMode.Point;
            }

            m_TerrainTexture = new Texture2D(m_Width, m_Height);
            m_TerrainMaterial.SetTexture("_HeightTexture", m_TerrainTexture);
            m_TerrainMaterial.SetFloat("_Height", m_Height);
            m_TerrainMaterial.SetFloat("_Width", m_Width);
        }

        void Update()
        {
            //regenerar perlins
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //clear all the childs of my transform
                foreach (Transform child in transform)
                {
                    Destroy(child.gameObject);
                }
                foreach (Transform child in props)
                {
                    Destroy(child.gameObject);
                }

                GenerateMapMLK();
                // GeneratePerlinMap();
                // SelectTexture(m_TextureOctave, m_TextureMode);
            }

            //canviar textura entre base i combinada
            // if (Input.GetKeyDown(KeyCode.C))
            //     SelectTexture(m_TextureOctave, (m_TextureMode + 1) % 2);


            if (Input.GetKeyDown(KeyCode.E))
            {
                m_OffsetY_temp = Random.Range(0, 1000);
                m_OffsetX_temp = Random.Range(0, 1000);
                m_OffsetY_height = Random.Range(0, 1000);
                m_OffsetX_height = Random.Range(0, 1000);
                m_OffsetX_rain = Random.Range(0, 1000);
                m_OffsetY_rain = Random.Range(0, 1000);
                montana.info.offsetX = Random.Range(0, 1000);
                montana.info.offsetY = Random.Range(0, 1000);
                cesped.info.offsetX = Random.Range(0, 1000);
                cesped.info.offsetY = Random.Range(0, 1000);
                arena.info.offsetX = Random.Range(0, 1000);
                arena.info.offsetY = Random.Range(0, 1000);
            }

            // //canviar l'octava de textura a mostrar
            // if (Input.GetKeyDown(KeyCode.Alpha0))
            //     SelectTexture(0, m_TextureMode);
            // if (Input.GetKeyDown(KeyCode.Alpha1))
            //     SelectTexture(1, m_TextureMode);
            // if (Input.GetKeyDown(KeyCode.Alpha2))
            //     SelectTexture(2, m_TextureMode);
            // if (Input.GetKeyDown(KeyCode.Alpha3))
            //     SelectTexture(3, m_TextureMode);
            // if (Input.GetKeyDown(KeyCode.Alpha4))
            //     SelectTexture(4, m_TextureMode);
            // if (Input.GetKeyDown(KeyCode.Alpha5))
            //     SelectTexture(5, m_TextureMode);
            // if (Input.GetKeyDown(KeyCode.Alpha6))
            //     SelectTexture(6, m_TextureMode);
            // if (Input.GetKeyDown(KeyCode.Alpha7))
            //     SelectTexture(7, m_TextureMode);
            // if (Input.GetKeyDown(KeyCode.Alpha8))
            //     SelectTexture(8, m_TextureMode);

        }

        private void GenerateMapMLK()
        {
            for (int y = 0; y < m_Height; y++)
            {
                for (int x = 0; x < m_Width; x++)
                {
                    // float perlin = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, m_Frequency, m_Width, m_Height,
                    //     m_OffsetX, m_OffsetY, m_Octaves, m_Lacunarity, m_Persistence, m_Carve);
                    float map = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, m_Frequency_height, m_Width,
                        m_Height,
                        m_OffsetX_height, m_OffsetY_height, m_Octaves, m_Lacunarity, m_Persistence, m_Carve);
                    float biometemp = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, m_Frequency_temp, m_Width,
                        m_Height,
                        m_OffsetX_temp, m_OffsetY_temp, m_Octaves, m_Lacunarity, m_Persistence, m_Carve);
                    float biomerainy = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, m_Frequency_rain, m_Width,
                        m_Height,
                        m_OffsetX_rain, m_OffsetY_rain, m_Octaves, m_Lacunarity, m_Persistence, m_Carve);
                    GameObject cubo = Instantiate(cube);
                    cubo.transform.parent = transform;
                    Debug.Log(map * multiply);
                    cubo.transform.position = new Vector3(x, (int)(map * multiply), y);
                    if (biometemp < .15f)
                    {
                        ChangeBiome(cubo,x,y,nieve); 
                    }

                    else if (biometemp < .5f)
                    {
                        if(biomerainy > .4f)ChangeBiome(cubo,x,y,montana);
                        else ChangeBiome(cubo,x,y,cesped);
                    }
                    else if (biometemp < .8f)
                    {
                        if(biomerainy > .8f)ChangeBiome(cubo,x,y,oceano);
                        else if(biomerainy > .3f)ChangeBiome(cubo,x,y,cesped);
                        else
                        {
                            if(biometemp < .65f)ChangeBiome(cubo,x,y,cesped);
                            else ChangeBiome(cubo,x,y,arena);
                        }
                    }
                    else
                    {
                        if(biomerainy > .5f)ChangeBiome(cubo,x,y,oceano);
                        else ChangeBiome(cubo,x,y,arena);
                    }
                    
                    if (actualBioma.info.props.Length > 0)
                    {
                        float spawnObjects = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, actualBioma.info.frequency, m_Width,
                            m_Height,
                            actualBioma.info.offsetX, actualBioma.info.offsetY);
                        //MENOS PROBABLE, Possiblemente bien en tema objetos
                        if (spawnObjects > .7f) SpawnProp(cubo, actualBioma.info.props[1]);
                        else SpawnProp(cubo, actualBioma.info.props[0]);
                        //BASTANTES MAS OBJETOS
                        // if (spawnObjects > .8f) SpawnProp(cubo, actualBioma.info.props[1]);
                        // else if (spawnObjects > .5f) SpawnProp(cubo, actualBioma.info.props[0]);
                    }

                }
            }
        }

        public void ChangeBiome(GameObject cubo, float x, float y, BiomaInfo material)
        {
            actualBioma = material;
            cubo.GetComponent<MeshRenderer>().material = actualBioma.info.color;
            // float altura = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, cesped.info.m_Frequency, m_Width,m_Height,
            //     m_OffsetX, m_OffsetY,cesped.info.m_Octaves,cesped.info.m_Lacunarity,cesped.info.m_Persistence,cesped.info.m_Carve)*20;
        }

        public void SpawnProp(GameObject cubo, GameObject prop)
        {
            int spawn = Random.Range(0, numMax);
            if (spawn == 0)
            {
                GameObject obj = Instantiate(prop);
                obj.transform.parent = props;
                obj.transform.position = cubo.transform.position + new Vector3(0,1,0);
            }
        }
    }
}

